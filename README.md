# youtube-dl-zenity
A (very) simple graphical user interface for [youtube-dl][1].

##About

youtube-dl-zenity is a simple bash script that provides a graphical user interface for the command-line program [youtube-dl][1]. All dialogs are created using [zenity][2].

Its intended purpose is to allow non-technical users to download and store videos and/or audio from [youtube][3] and other video hosting services.

##Screenshots

![Enter URL Dialog](screenshots/enter_url_dialog.png)
![Extract Audio Dialog](screenshots/extract_audio_dialog.png)

##Dependencies
* bash
* youtube-dl
* xterm
* zenity

##Author

Copyright 2016 Daniel James <dj (at) slashspace (dot) org>

##License

youtube-dl-zenity is released under the GNU General Public License, version 3 or later (see LICENSE).

[1]: https://rg3.github.io/youtube-dl/
[2]: https://wiki.gnome.org/action/show/Projects/Zenity
[3]: https://www.youtube.com
