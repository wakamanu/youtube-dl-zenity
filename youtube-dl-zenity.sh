#!/bin/bash


URL=$(zenity --title "Enter URL" --entry --text "Enter URL:")
if [[ -z "$URL" ]]; then
    echo "Operation canceled (empty URL)."
    exit 2
fi
CMD="youtube-dl --no-overwrites"

zenity --question --text="Do you want to extract the audio signal from the video (will create an additional file)?" --cancel-label="No" --ok-label="Yes"
EXTRACT_AUDIO=$?
if [[ $EXTRACT_AUDIO -eq 0 ]]; then
    CMD="$CMD --extract-audio"
fi

CMD="$CMD $URL"
exec xterm -hold -e "$CMD"
exit 0
